포트폴리오 Git입니다.
==========================

1. 여행 정보 프로젝트

- 제 3회 백석대학교 산학협력단 링크플러스 지능형 소프트웨어 성과발표회 우수상 수상

- 설계 목적 설정 : 학부내에서 팀프로젝트를 진행하여 팀원 전체의 실력 향상을 목표로 작성함.
- 구현 방법 결정 : 서버와 데이터베이스는 아마존 웹 서비스를 이용하여 클라우드 환경에서 개발하도록 함. 사용될 언어와 프레임워크 자바의 스프링 프레임워크를 사용하기로 함.
- 프로젝트에서의 나의 역할 : 아마존 RDS, EC2 , S3 서버 구축 및 DB 설계 및 쿼리문 작성과 S3 버켓의 파일 업로드 부분의 백엔드단 작성. 스프링 프레임워크와 AWS RDS, EC2, S3 를 이용하여 B2C형태 웹 프로젝트를 개발하여 제 3회 지능형 소프트웨어 성과발표회 우수상 수상.
AWS 프리티어를 극한으로 이용하기 위해 EC2 위에 DBMS를 설치하지 않고 프리티어 RDS 인스턴스를 생성하여 Oracle 인스턴스를 생성하여 서버와 데이터베이스 서버를 분리시켜 속도와 안전성을 해결하도록 노력함.
데이터베이스의 부담을 줄이기 위해 여행 이미지를 BROB 형태로 저장 하지 않고 S3 스토리지에 저장 하는 방식을 사용하였고 AWS 프리티어에 3개의 인스턴스를 활용 함에도 요금을 내지 않도록 설계함. 1Ghz CPU, 1GB RAM 등의 성능을 가진 낮은 성능의 3개의 인스턴스를 이용하여 서버, 데이터베이스, 스토리지를 분산시켜 웹 서비스의 속도와 안정성을 끌어 올림.
- 사이트의 구조와 소스를 이용하여 다른 웹 프로젝트를 생성할 수 있도록 처음부터 설계하여 이후 다른 프로젝트를 진행함에도 구성이 쉽도록 설계 문서 및 코드를 정리함.
- ERD CLOUD 를 이용하여 데이터베이스를 한눈에 보기 편하게 설계를 하여 팀원들의 데이터베이스 이해도를 향상토록 하며 데이터베이스의 수정하기 전 ERD 클라우드에 미리 적용을 하여 백엔드를 구성하는 팀원의 혼선을 최소화 하도록 노력함.
( 링크 : https://www.erdcloud.com/d/WHzXSXHaNTFHu7rbc )
- 초기설계 단계에서 팀원의 능력과 기술 스택에 맞게 프로젝트를 분배함
EC2와 RDS와 데이터모델링을 통하여 팀원들이 프로젝트에 전념 할 수 있도록 함.
- 완성된 프로젝트는 프리티어 기간인 365일간 무료이며 365일 24시간 항상 접속이 가능함.
( 링크 : http://18.221.37.6:8080/TravelProject/project/index.do )
- 모든 소스코드는 Gitlab에 공개.
( 링크 : https://gitlab.com/No1_Korean_Developer/portpolio )
- AWS Cloudwatch를 이용하여 요금 발생시 대처 할 수 있도록 대비함. 또한 모든 서버 로그는 S3 스토리지에 자동으로 저장함.

2. 데이터 사이언스
- 네이버 뉴스 웹 크롤링
- 크롬 웹 드라이버를 사용한 웹 크롤링
- 부동산 데이터와 공시가격의 상관관계 분석
- 응급수술 환자를 위한 의료기관 위치 분석
- 의료기관 병상 및 의료인 분석
- 의료데이터 분석을 통한 심장 질환 유발률 비지도학습(unsupervised learning)과 군집(clustering)

3. (주)대화기기의 에서의 작성한 임베디드 기반의 의약품 주입펌프 DI-6200V의 소스코드는 (주)대화기기가 저작권을 가지고 있기 떄문에 Public Git에 올릴 수 없는 점 양해부탁드립니다.
